package com.example.kiptum.application01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText text1, text2;
    private Button Convert;
    private Button Exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        //getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton() {
        text1 = findViewById(R.id.editText1);
        text2 = findViewById(R.id.editText2);
        Convert = findViewById(R.id.button);
        Exit = findViewById(R.id.button2);

        Convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if("".equals(text2.getText().toString())) {
                    String value = text1.getText().toString();
                    int v = Integer.parseInt(value);
                    double convert = v / 25.4;
                    text2.setText(String.valueOf(convert));
                    text2.setEnabled(false);
                    Toast.makeText(getApplicationContext(), text1.getText().toString()+" Millimetres is equal to "+String.valueOf(convert)+" Inches", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Please enter Millimetres to Convert to Inches", Toast.LENGTH_LONG).show();
                }
            }
        });
        Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}